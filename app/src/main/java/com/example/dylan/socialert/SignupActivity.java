package com.example.dylan.socialert;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class SignupActivity extends AppCompatActivity {

    private final Logger logger = Logger.getAnonymousLogger();
    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
    }

    public void gotoLoginIntent(View view) {
        Intent loginIntent = new Intent(this, MainActivity.class);
        String intentParam = "Param2-string";
        loginIntent.putExtra("param2", intentParam);
        startActivity(loginIntent);
    }

    public void signup(View view) {
        EditText emailInput = (EditText) findViewById(R.id.input_email);
        EditText passwordInput = (EditText) findViewById(R.id.input_password);
        EditText usernameInput = (EditText) findViewById(R.id.input_username);
        EditText phoneInput = (EditText) findViewById(R.id.input_phone);
        CheckBox twoFactor = (CheckBox) findViewById(R.id.two_factor_checkbox);

        String email = emailInput.getText().toString();
        String password = passwordInput.getText().toString();
        String username = usernameInput.getText().toString();
        String phoneText = phoneInput.getText().toString();

        if (twoFactor.isChecked())
            logger.info("User has enabled two factor authentication");
        else
            logger.info("User has not enabled two factor authentication");

        sendSignupCredentials(email, password, username, phoneText, twoFactor.isChecked());

    }

    private void gotoProfile() {
        Intent profileIntent = new Intent(this, ProfileActivity.class);
        startActivity(profileIntent);
    }

    private void sendSignupCredentials(String email, String password, String username, String phoneNumber, boolean twoFactorEnabled) {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = "http://chatappbackend.appspot.com/register";
        Logger logger = Logger.getAnonymousLogger();

        Map<String,String> params = new HashMap<String, String>();
        params.put("register_type", "register-droid");
        params.put("email", email);
        params.put("password", password);
        params.put("username", username);
        params.put("phone", phoneNumber);

        JSONObject paramsJson = new JSONObject(params);

        logger.info(paramsJson.toString());

        // Request a string response from the provided URL.
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, paramsJson,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        logger.info("[+] Response from server");

                        try {
                            logger.info(response.toString());
                            if (response.has("exception") && response.getBoolean("exception"))
                                displayToast("Server Error");
                            else if (response.has("error") && response.getBoolean("error"))
                                displayToast(response.getString("message"));
                            else if (response.has("success") && response.getBoolean("success"))
                                gotoProfile();
                            else
                                displayToast("Invalid Sign up data");

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.info("[+] Error ");
                        logger.info(error.toString());
                    }
                }
        );

        // Add the request to the RequestQueue.
        queue.add(jsonRequest);
    }

    private void displayToast(String toastText) {
        Toast toast = Toast.makeText(getApplicationContext(), toastText, Toast.LENGTH_LONG);
        toast.show();
    }
}
