package com.example.dylan.socialert;


import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.Loader;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import android.content.Intent;
import android.util.Pair;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.UrlEncodedContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.Json;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.JsonParser;
import com.google.api.client.util.LoggingByteArrayOutputStream;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;
import org.w3c.dom.Text;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * A login screen that offers login via email/password.
 */
public class MainActivity extends AppCompatActivity {

    private final String TAG = "LoginActivity";
    private final int REQUEST_SIGNUP = 0;
    private Logger logger = Logger.getAnonymousLogger();
    private URL chatBackendProductionUrl;
    private URL chatBackendDevelopmentUrl;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        logger.info("[+] Launching Login Activity onCreate()");
        try {
            chatBackendDevelopmentUrl = new URL("http://localhost:8080");
//            chatBackendProductionUrl = new URL("https://chatappbackend.appspot.com");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void login(View view) {
        EditText usernameInput = (EditText) findViewById(R.id.input_username);
        String usernameText = usernameInput.getText().toString();

        EditText passwordInput = (EditText) findViewById(R.id.input_password);
        String passwordText = passwordInput.getText().toString();

        sendLoginCredentials(usernameText, passwordText);
    }

    public void gotoSignupIntent(View view) {
        Intent signupIntent = new Intent(this, SignupActivity.class);
        startActivity(signupIntent);
    }

    public void sendLoginCredentials(String username, String password) {

        try {

            // Instantiate the RequestQueue.
            RequestQueue queue = Volley.newRequestQueue(this);
            String url = "http://chatappbackend.appspot.com/login";
            Logger logger = Logger.getAnonymousLogger();

            Map<String,String> params = new HashMap<String, String>();
            params.put("login_type", "un-pw-droid");
            params.put("un", username);
            params.put("pw", password);

            JSONObject paramsJson = new JSONObject(params);

            logger.info(paramsJson.toString());

            // Request a string response from the provided URL.
            JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, paramsJson,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        logger.info("[+] Response from server");
                        try {
                            if (response.has("exception") && response.getBoolean("exception")) {
                                displayToast("Server error");
                                return;
                            } else if (response.has("error") && response.getBoolean("error")) {
                                displayToast(response.getString("message"));
                            } else if (response.has("logged_in") && response.getBoolean("logged_in")) {
                                gotoProfile();
                            } else {
                                displayToast("Invalid Login");
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        logger.info("[+] Error ");
                        logger.info(error.toString());
                    }
                }
            );

            // Add the request to the RequestQueue.
            queue.add(jsonRequest);

        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("[+] Error sending request to development server");
        }
    }

    private void displayToast(String toastText) {
        Toast toast = Toast.makeText(getApplicationContext(), toastText, Toast.LENGTH_LONG);
        toast.show();
    }

    private void gotoProfile() {
        Intent profileIntent = new Intent(this, ProfileActivity.class);
        startActivity(profileIntent);
    }
}
